##############################################################################
cd ~/projects/
git clone git@bitbucket.org:arcamens/slock.git slock-code
##############################################################################

# push, slock, master.
cd ~/projects/django-slock-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;

git status
git add *
git commit -a
git push -u origin master
##############################################################################
# create dev branch.
cd ~/projects/slock-code
git branch -a
git checkout -b development
git push --set-upstream origin development
##############################################################################
# create releases.

git tag -a 0.0.1 -m 'Initial release'
git push origin : 0.0.1
##############################################################################

git tag -a 1.1.0 
git push origin 1.1.0



from django import forms
from django.conf import settings
from slock.models import BasicUser
from traceback import print_exc
from django.db import models

class LoginForm(forms.Form):
    email    = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, session=None, *args, **kwargs):
        self.session = session
        super(LoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(LoginForm, self).clean()
        email    = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        try:
            user = BasicUser.objects.get(email = email, password=password)
        except BasicUser.DoesNotExist as e:
            print_exc()
            raise forms.ValidationError(
                'User or password is wrong!')
        else:
            self.session['user_id'] = user.id

class SetPasswordForm(forms.ModelForm):
    retype   = forms.CharField(required=True, widget=forms.PasswordInput())
    password = forms.CharField(required=True, widget=forms.PasswordInput())

    def clean(self):
        super(SetPasswordForm, self).clean()
        retype   = self.cleaned_data.get('retype')
        password = self.cleaned_data.get('password')

        if retype != password:
            raise forms.ValidationError(
                'Passwords dont match!')

    class Meta:
        model  = BasicUser
        fields = ('password', )

class UpdatePasswordForm(SetPasswordForm):
    old = forms.CharField(required=True, widget=forms.PasswordInput())
    def clean(self):
        super(UpdatePasswordForm, self).clean()
        old = self.cleaned_data.get('old')
        
        if old != self.instance.password:
            raise forms.ValidationError("Wrong existing password!")



